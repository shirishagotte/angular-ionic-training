import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'hello-world';
  childData: string;
  parentMethod(data){
    this.childData = data;
  }

  employees: any[] = [
    {name:'Shirisha', empid:2967},
    {name:'Swati', empid:2966},
    {name:'Srivalli', empid:2969},
    {name:'Venu', empid:2977},
    {name:'Vijay', empid:2970},
    {name:'Swaroop', empid:2961}
  ];

}
