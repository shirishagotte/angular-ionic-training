import { EmployeesService } from './../employees.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent {

title = "List of Employees";
employees;

constructor(empService: EmployeesService){
  this.employees = empService.getEmployees();
}

}
