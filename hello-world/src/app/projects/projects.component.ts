import { ProjectsService } from './../projects.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-projects',
  template: `
  <h2>{{title}}<h2>
    <ul>
      <li *ngFor="let project of projects">
        {{project}}
      </li>
    </ul>
  `,
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent {

  title = "List of projects"
  projects;

  constructor(service: ProjectsService){
    this.projects = service.getProjects();
  }
}
