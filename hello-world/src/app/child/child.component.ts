import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-child',
  template: `
  <div>
  <h3> This is child component</h3>
  <button (click)="passData()">Send data back to parent</button>
  </div>
  `,
  styleUrls: ['./child.component.css']
})
export class ChildComponent {
  @Output()
  notify: EventEmitter<string> = new EventEmitter<string>();

  passData(){
    this.notify.emit("This message is coming from child component");
  }
}
