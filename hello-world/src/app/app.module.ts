import { StudentModule } from './student/student.module';
import { ChildComponent } from './child/child.component';
import { EmployeesService } from './employees.service';
import { CoursesService } from './courses.service';
import { ProjectsService } from './projects.service';
import { CoursesComponent } from './courses.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CourseComponent } from './course/course.component';
import { ProjectsComponent } from './projects/projects.component';
import { ProjectDetailsComponent } from './projects/project-details/project-details.component';
import { EmployeesComponent } from './employees/employees.component';
import { EmployeeDetailsComponent } from './employees/employee-details/employee-details.component';
import { BindingComponent } from './binding/binding.component';
import {FormsModule} from '@angular/forms';
import { RedblackDirective } from './redblack.directive';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    CoursesComponent,
    CourseComponent,
    ProjectsComponent,
    ProjectDetailsComponent,
    EmployeesComponent,
    EmployeeDetailsComponent,
    ChildComponent,
    BindingComponent,
    RedblackDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    StudentModule   
  ],
  providers: [ProjectsService, CoursesService, EmployeesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
