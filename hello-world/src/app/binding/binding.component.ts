import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-binding',
  templateUrl: './binding.component.html',
  styleUrls: ['./binding.component.css']
})
export class BindingComponent {

  pageTitle: string = "Data binding in angular";

  changeTitle(){
    this.pageTitle = "Data binding";
  }

}
